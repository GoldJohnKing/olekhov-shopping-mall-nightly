#!/bin/sh

# Get current file path
CURRENT_PATH=$(dirname $(readlink -f "$0"))

echo "Switch to gnome-terminal..."
gnome-terminal --tab -t 'Onekey Package' --working-directory=${CURRENT_PATH} -- bash -c 'bash ./onekey_package.sh; exec zsh'

sleep 3
echo "This window could be closed after gnome-terminal started."
