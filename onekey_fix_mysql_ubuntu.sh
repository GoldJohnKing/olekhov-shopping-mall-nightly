#!/bin/sh

echo "Onekey Fix Mysql on Ubuntu..."

# Fix /var/run/mysql folder and its permission
echo "Fix /var/run/mysql folder and its permission"
sudo mkdir /var/run/mysql
sudo chown -R mysql.mysql /var/run/mysql

# Fix /var/log/mysql folder and its permission
echo "Fix /var/log/mysql folder and its permission"
sudo mkdir /var/log/mysql
sudo chown -R mysql.mysql /var/log/mysql 

echo "Onekey Fix Mysql on Ubuntu Complete."

exec zsh

