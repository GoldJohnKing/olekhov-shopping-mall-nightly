#!/bin/sh

echo "Onekey Shutdown..."

# Get current file path
CURRENT_PATH=$(dirname $(readlink -f "$0"))

# Shutdown Sentinel Dashboard
echo "Shutdown Sentinel Dashboard..."
jps -v | grep "sentinel-dashboard-1.7.1.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Sentinel Dashboard Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Zipkin Server
echo "Shutdown Zipkin Server..."
jps -v | grep "zipkin-server-2.21.0-exec.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Zipkin Server Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Microservice Spring Boot Admin
echo "Shutdown Microservice Spring Boot Admin Server..."
jps -v | grep "spring-boot-admin-server-0.0.1-SNAPSHOT.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Microservice Spring Boot Admin Server Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Microservice Gateway
echo "Shutdown Microservice Gateway..."
jps -v | grep "gateway-0.0.1-SNAPSHOT.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Microservice Gateway Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Nacos Server
echo "Shutdown Nacos Server..."
${CURRENT_PATH}/nacos/bin/shutdown.sh
echo "Nacos Server Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown RocketMQ Console
echo "Shutdown RocketMQ Console..."
jps -v | grep "rocketmq-console-ng-1.0.1.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "RocketMQ Console Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown RocketMQ Broker
echo "Shutdown RocketMQ Broker..."
${CURRENT_PATH}/rocketmq-all-4.5.1-bin-release/bin/mqshutdown broker
echo "RocketMQ Broker Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown RocketMQ Name Server
echo "Shutdown RocketMQ Name Server..."
${CURRENT_PATH}/rocketmq-all-4.5.1-bin-release/bin/mqshutdown namesrv
echo "RocketMQ Name Server Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Microservice Comment-Center
echo "Shutdown Microservice Comment-Center..."
jps -v | grep "comment-center-0.0.1-SNAPSHOT.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Microservice Comment-Center Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Microservice Cart-Center
echo "Shutdown Microservice Cart-Center..."
jps -v | grep "cart-center-0.0.1-SNAPSHOT.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Microservice Cart-Center Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Microservice Billing-Center
echo "Shutdown Microservice Billing-Center..."
jps -v | grep "billing-center-0.0.1-SNAPSHOT.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Microservice Billing-Center Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Microservice Item-Center
echo "Shutdown Microservice Item-Center..."
jps -v | grep "item-center-0.0.1-SNAPSHOT.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Microservice Item-Center Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown Microservice User-Center
echo "Shutdown Microservice User-Center..."
jps -v | grep "user-center-0.0.1-SNAPSHOT.jar" | awk '{print $1}'|xargs kill -s SIGINT > /dev/null 2>&1
echo "Microservice User-Center Shutdown.\nSleep for 5 seconds..."
sleep 5

# Shutdown MySQL Server
echo "Shutdown MySQL Server..."
systemctl stop mysql

echo "Onekey Shutdown Complete."

exec zsh
