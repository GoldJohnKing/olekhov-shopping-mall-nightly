#!/bin/sh

echo "Onekey Startup Microservices..."

# Get current file path
CURRENT_PATH=$(dirname $(readlink -f "$0"))

# Start Microservice Gateway
echo "Starting Microservice Gateway..."
gnome-terminal --tab -t 'Gateway' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/gateway/target/gateway-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Gateway started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice User-Center
echo "Starting Microservice User-Center..."
gnome-terminal --tab -t 'User-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/user-center/target/user-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice User-Center started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice Item-Center
echo "Starting Microservice Item-Center..."
gnome-terminal --tab -t 'Item-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/item-center/target/item-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Item-Center started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice Cart-Center
echo "Starting Microservice Cart-Center..."
gnome-terminal --tab -t 'Cart-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/cart-center/target/cart-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Cart-Center started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice Billing-Center
echo "Starting Microservice Billing-Center..."
gnome-terminal --tab -t 'Billing-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/billing-center/target/billing-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Billing-Center started."
sleep 5

# Start Microservice Comment-Center
echo "Starting Microservice Comment-Center..."
gnome-terminal --tab -t 'Comment-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/comment-center/target/comment-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Comment-Center started.\nSleep for 5 seconds..."
sleep 5

echo "Onekey Startup Microservices Complete."

exec zsh
