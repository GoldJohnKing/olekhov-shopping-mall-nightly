#!/bin/sh

echo "Onekey Package..."

# Get current file path
CURRENT_PATH=$(dirname $(readlink -f "$0"))

# Package Microservice Gateway
echo "Packaging Microservice Gateway..."
gnome-terminal --tab -t 'Gateway' --working-directory=${CURRENT_PATH}/IdeaProjects/gateway -- bash -c 'bash /home/gjk/DevTools/apache-maven-3.6.3/bin/mvn clean package; exec zsh'
echo "Microservice Gateway Packaged.\nSleep for 1 seconds..."
sleep 1

# Package Microservice Spring Boot Admin
echo "Packaging Microservice Spring Boot Admin Server..."
gnome-terminal --tab -t 'Spring Boot Admin Server' --working-directory=${CURRENT_PATH}/IdeaProjects/spring-boot-admin-server -- bash -c 'bash /home/gjk/DevTools/apache-maven-3.6.3/bin/mvn clean package; exec zsh'
echo "Microservice Spring Boot Admin Server Packaged.\nSleep for 1 seconds..."
sleep 1

# Package Microservice Comment-Center
echo "Packaging Microservice Comment-Center..."
gnome-terminal --tab -t 'Comment-Center' --working-directory=${CURRENT_PATH}/IdeaProjects/comment-center -- bash -c 'bash /home/gjk/DevTools/apache-maven-3.6.3/bin/mvn clean package; exec zsh'
echo "Microservice Comment-Center Packaged.\nSleep for 1 seconds..."
sleep 1

# Package Microservice Billing-Center
echo "Packaging Microservice Billing-Center..."
gnome-terminal --tab -t 'Billing-Center' --working-directory=${CURRENT_PATH}/IdeaProjects/billing-center -- bash -c 'bash /home/gjk/DevTools/apache-maven-3.6.3/bin/mvn clean package; exec zsh'
echo "Microservice Billing-Center Packaged.\nSleep for 1 seconds..."
sleep 1

# Package Microservice Cart-Center
echo "Packaging Microservice Cart-Center..."
gnome-terminal --tab -t 'Cart-Center' --working-directory=${CURRENT_PATH}/IdeaProjects/cart-center -- bash -c 'bash /home/gjk/DevTools/apache-maven-3.6.3/bin/mvn clean package; exec zsh'
echo "Microservice Cart-Center Packaged.\nSleep for 1 seconds..."
sleep 1

# Package Microservice Item-Center
echo "Packaging Microservice Item-Center..."
gnome-terminal --tab -t 'Item-Center' --working-directory=${CURRENT_PATH}/IdeaProjects/item-center -- bash -c 'bash /home/gjk/DevTools/apache-maven-3.6.3/bin/mvn clean package; exec zsh'
echo "Microservice Item-Center Packaged.\nSleep for 1 seconds..."
sleep 1

# Package Microservice User-Center
echo "Packaging Microservice User-Center..."
gnome-terminal --tab -t 'User-Center' --working-directory=${CURRENT_PATH}/IdeaProjects/user-center -- bash -c 'bash /home/gjk/DevTools/apache-maven-3.6.3/bin/mvn clean package; exec zsh'
echo "Microservice User-Center Packaged."
sleep 1

echo "Onekey Package Complete."

exec zsh
