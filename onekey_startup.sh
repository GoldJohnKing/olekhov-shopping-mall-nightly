#!/bin/sh

echo "Onekey Startup..."

# Get current file path
CURRENT_PATH=$(dirname $(readlink -f "$0"))

# Start MySQL Server
echo "Start MySQL Server..."
systemctl start mysql
echo "MySQL Server started.\nSleep for 5 seconds..."
sleep 5

# Start Nacos Server
echo "Starting Nacos Server Standalone Mode..."
gnome-terminal --tab -t 'Nacos Server' --working-directory=${CURRENT_PATH} -- bash -c 'bash ./nacos/bin/startup.sh -m standalone; exec zsh'
echo "Nacos Server Standalone Mode started.\nSleep for 5 seconds..."
sleep 5

# Start Sentinel Dashboard
echo "Starting Sentinel Dashboard..."
gnome-terminal --tab -t 'Sentinel Dashboard' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./sentinel/sentinel-dashboard-1.7.1.jar; exec zsh'
echo "Sentinel Dashboard started.\nSleep for 5 seconds..."
sleep 5

# Start RocketMQ Name Server
echo "Starting RocketMQ Name Server..."
gnome-terminal --tab -t 'RocketMQ Name Server' --working-directory=${CURRENT_PATH} -- bash -c './rocketmq-all-4.5.1-bin-release/bin/mqnamesrv; exec zsh'
echo "RocketMQ Name Server started.\nSleep for 5 seconds..."
sleep 5

# Start RocketMQ Broker
echo "Starting RocketMQ Broker..."
gnome-terminal --tab -t 'RocketMQ Broker' --working-directory=${CURRENT_PATH} -- bash -c './rocketmq-all-4.5.1-bin-release/bin/mqbroker -n localhost:9876; exec zsh'
echo "RocketMQ Broker started.\nSleep for 5 seconds..."
sleep 5

# Start RocketMQ Console with OpenJDK-8
echo "Starting RocketMQ Console with OpenJDK-8..."
gnome-terminal --tab -t 'RocketMQ Console' --working-directory=${CURRENT_PATH} -- bash -c '/usr/lib/jvm/java-8-openjdk-amd64/bin/java -jar ./rocketmq-console/rocketmq-console-ng-1.0.1.jar; exec zsh'
echo "RocketMQ Console with OpenJDK-8 started."

# Start Zipkin Server
echo "Starting Zipkin Server..."
gnome-terminal --tab -t 'Zipkin Server' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./zipkin/zipkin-server-2.21.0-exec.jar; exec zsh'
echo "Zipkin Server started."

echo "Sleep for 5 seconds before starting Microservices..."
sleep 5

# Start Microservice Gateway
echo "Starting Microservice Gateway..."
gnome-terminal --tab -t 'Gateway' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/gateway/target/gateway-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Gateway started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice Spring Boot Admin
echo "Starting Microservice Spring Boot Admin Server..."
gnome-terminal --tab -t 'Spring Boot Admin Server' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/spring-boot-admin-server/target/spring-boot-admin-server-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Spring Boot Admin Server started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice User-Center
echo "Starting Microservice User-Center..."
gnome-terminal --tab -t 'User-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/user-center/target/user-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice User-Center started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice Item-Center
echo "Starting Microservice Item-Center..."
gnome-terminal --tab -t 'Item-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/item-center/target/item-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Item-Center started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice Cart-Center
echo "Starting Microservice Cart-Center..."
gnome-terminal --tab -t 'Cart-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/cart-center/target/cart-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Cart-Center started.\nSleep for 5 seconds..."
sleep 5

# Start Microservice Billing-Center
echo "Starting Microservice Billing-Center..."
gnome-terminal --tab -t 'Billing-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/billing-center/target/billing-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Billing-Center started."
sleep 5

# Start Microservice Comment-Center
echo "Starting Microservice Comment-Center..."
gnome-terminal --tab -t 'Comment-Center' --working-directory=${CURRENT_PATH} -- bash -c 'java -jar ./IdeaProjects/comment-center/target/comment-center-0.0.1-SNAPSHOT.jar; exec zsh'
echo "Microservice Comment-Center started.\nSleep for 5 seconds..."
sleep 5

echo "Onekey Startup Complete."

exec zsh
