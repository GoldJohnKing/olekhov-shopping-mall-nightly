package com.olekhov.billingcenter.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BillingDTO {
    private Long id;
    private Long itemId;
    private String itemName;
    private Integer merchantId;
    private String merchantName;
    private Integer customerId;
    private String customerName;
    private BigDecimal totalPrice;
    private Integer count;
    private String status;
}