package com.olekhov.billingcenter.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BillingInsertDTO {
    private Long itemId;
    private Integer customerId;
    private Integer count;
}
