package com.olekhov.billingcenter.service.billing;

import com.alibaba.fastjson.JSON;
import com.olekhov.billingcenter.dao.billing.BillingMapper;
import com.olekhov.billingcenter.dao.billing.RocketmqTransactionLogMapper;
import com.olekhov.billingcenter.domain.dto.*;
import com.olekhov.billingcenter.domain.entity.billing.Billing;
import com.olekhov.billingcenter.domain.entity.billing.RocketmqTransactionLog;
import com.olekhov.billingcenter.feignclient.ItemCenterFeignClient;
import com.olekhov.billingcenter.feignclient.UserCenterFeignClient;
import lombok.RequiredArgsConstructor;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillingService {
    private final BillingMapper billingMapper;
    private final UserCenterFeignClient userCenterFeignClient;
    private final ItemCenterFeignClient itemCenterFeignClient;
    private final RocketmqTransactionLogMapper rocketmqTransactionLogMapper;
    private final Source source;

    public BillingDTO selectById(Long id) {
        Billing billing = this.billingMapper.selectByPrimaryKey(id);
        // Assemble DTO
        BillingDTO billingDTO = new BillingDTO();
        BeanUtils.copyProperties(billing, billingDTO);
        return billingDTO;
    }

    public List<BillingDTO> listByUserId(Example example){
        // Fetch billing list from database
        List<Billing> billingList = this.billingMapper.selectByExample(example);
        // Batch assemble DTO
        List<BillingDTO> billingDTOList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(billingList)) {
            for (Billing billing : billingList) {
                // Assemble DTO
                BillingDTO billingDTO = new BillingDTO();
                BeanUtils.copyProperties(billing, billingDTO);
                billingDTOList.add(billingDTO);
            }
        }
        return billingDTOList;
    }

    public List<BillingDTO> listByMerchantId(Integer merchantId) {
        // Define query example
        Example example = new Example(Billing.class);
        example.createCriteria().andEqualTo("merchantId", merchantId);
        return this.listByUserId(example);
    }

    public List<BillingDTO> listByCustomerId(Integer customerId) {
        // Define query example
        Example example = new Example(Billing.class);
        example.createCriteria().andEqualTo("customerId", customerId);
        return this.listByUserId(example);
    }

    public Billing insert(BillingInsertDTO billingInsertDTO) {
        // TODO: Get current user's id
        // TODO: Check if current user has customer permission
        // Fetch needed info
        UserDTO customerDTO = this.userCenterFeignClient.selectById(billingInsertDTO.getCustomerId());
        ItemDTO itemDTO = this.itemCenterFeignClient.selectById(billingInsertDTO.getItemId());
        UserDTO merchantDTO = this.userCenterFeignClient.selectById(itemDTO.getMerchantId());
        // Assemble billing
        Billing billing = new Billing();
        BeanUtils.copyProperties(billingInsertDTO, billing);
        billing.setItemName(itemDTO.getName());
        billing.setMerchantId(itemDTO.getMerchantId());
        billing.setMerchantName(merchantDTO.getUsername());
        billing.setCustomerName(customerDTO.getUsername());
        // Parse Count: must not null & >= 1
        if (billingInsertDTO.getCount() == null) billingInsertDTO.setCount(1);
        billingInsertDTO.setCount(Math.max(billingInsertDTO.getCount(),1));
        billing.setCount(billingInsertDTO.getCount());
        billing.setTotalPrice(
                itemDTO.getPrice().multiply(
                        BigDecimal.valueOf(billingInsertDTO.getCount())
                )
        );
        // Send async messages
        this.source.output().send(
                MessageBuilder.withPayload(
                        RemovePurchasedCartItemDTO.builder()
                                .itemId(billing.getItemId())
                                .customerId(billing.getCustomerId())
                                .build()
                )
                        .setHeader(RocketMQHeaders.TRANSACTION_ID, UUID.randomUUID().toString())
                        // Serialization: Pass Billing to TransactionListener as a JSON String in header
                        .setHeader("billing", JSON.toJSONString(billing))
                        .build()
        );
        return billing;
    }

    // Async method to remove purchased items in then submitting RocketMQ Log:
    @Transactional(rollbackFor = Exception.class)
    public void insertNewBillingWithRocketMQLog(Billing billing, String transactionId) {
        this.billingMapper.insertSelective(billing);
        // Log transaction finished status in database
        this.rocketmqTransactionLogMapper.insertSelective(
                RocketmqTransactionLog.builder()
                        .transactionId(transactionId)
                        .log("Remove purchased items in cart while inserting billing.")
                        .build()
        );
        // TODO: Write billing to cache
    }

    public Billing updateStatusById(Long id, BillingStatusUpdateDTO billingStatusUpdateDTO) {
        // TODO: Get current user's id
        // TODO: Check if current user has valid permission
        Billing billing = this.billingMapper.selectByPrimaryKey(id);
        // Check if billing exists
        if (billing == null) throw new IllegalArgumentException("Invalid Parameters: Billing does not exist.");
        // Assemble billing
        BeanUtils.copyProperties(billingStatusUpdateDTO, billing);
        // Submit changes
        this.billingMapper.updateByPrimaryKeySelective(billing);
        return billing;
    }

}
