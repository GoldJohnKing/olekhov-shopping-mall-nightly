package com.olekhov.billingcenter.domain.dto;

import com.olekhov.billingcenter.domain.enums.BillingStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BillingStatusUpdateDTO {
    private Long id;
    private Enum<BillingStatusEnum> status;
}
