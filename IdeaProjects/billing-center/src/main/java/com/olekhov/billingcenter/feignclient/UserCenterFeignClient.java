package com.olekhov.billingcenter.feignclient;

import com.olekhov.billingcenter.domain.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "user-center", fallbackFactory = UserCenterFeignClientFallbackFactory.class)
public interface UserCenterFeignClient {
    @GetMapping("/user/select/{id}")
    UserDTO selectById(@PathVariable Integer id);
}
