package com.olekhov.billingcenter.dao.billing;

import com.olekhov.billingcenter.domain.entity.billing.RocketmqTransactionLog;
import tk.mybatis.mapper.common.Mapper;

public interface RocketmqTransactionLogMapper extends Mapper<RocketmqTransactionLog> {
}