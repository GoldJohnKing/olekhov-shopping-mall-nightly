package com.olekhov.billingcenter.feignclient;

import com.olekhov.billingcenter.domain.dto.ItemDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "item-center", fallbackFactory = ItemCenterFeignClientFallbackFactory.class)
public interface ItemCenterFeignClient {
    @GetMapping("/item/select/{id}")
    ItemDTO selectById(@PathVariable Long id);
}
