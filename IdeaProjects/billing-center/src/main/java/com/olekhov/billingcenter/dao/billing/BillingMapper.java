package com.olekhov.billingcenter.dao.billing;

import com.olekhov.billingcenter.domain.entity.billing.Billing;
import tk.mybatis.mapper.common.Mapper;

public interface BillingMapper extends Mapper<Billing> {
}