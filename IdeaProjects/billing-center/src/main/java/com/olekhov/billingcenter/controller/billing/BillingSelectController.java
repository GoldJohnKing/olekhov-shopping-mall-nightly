package com.olekhov.billingcenter.controller.billing;

import com.olekhov.billingcenter.domain.dto.BillingDTO;
import com.olekhov.billingcenter.service.billing.BillingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/billing")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillingSelectController {
    private final BillingService billingService;

    @GetMapping("/select/{id}")
    public BillingDTO selectById(@PathVariable Long id) {
        return this.billingService.selectById(id);
    }

}
