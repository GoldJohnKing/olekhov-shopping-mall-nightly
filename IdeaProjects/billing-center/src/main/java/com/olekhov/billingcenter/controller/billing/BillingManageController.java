package com.olekhov.billingcenter.controller.billing;

import com.olekhov.billingcenter.domain.dto.BillingInsertDTO;
import com.olekhov.billingcenter.domain.dto.BillingStatusUpdateDTO;
import com.olekhov.billingcenter.domain.entity.billing.Billing;
import com.olekhov.billingcenter.service.billing.BillingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/billing")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillingManageController {
    private final BillingService billingService;

    @PutMapping("/add")
    public Billing insert(@RequestBody BillingInsertDTO billingInsertDTO) {
        return this.billingService.insert(billingInsertDTO);
    }

    @PutMapping("/edit/{id}")
    public Billing updateById(@PathVariable Long id, @RequestBody BillingStatusUpdateDTO billingStatusUpdateDTO) {
        return this.billingService.updateStatusById(id, billingStatusUpdateDTO);
    }
}
