package com.olekhov.billingcenter.domain.entity.billing;

import javax.persistence.*;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ROCKETMQ_TRANSACTION_LOG")
public class RocketmqTransactionLog {
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    @Column(name = "TRANSACTION_ID")
    private String transactionId;

    @Column(name = "LOG")
    private String log;
}