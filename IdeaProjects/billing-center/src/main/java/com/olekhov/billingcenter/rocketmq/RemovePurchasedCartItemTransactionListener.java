package com.olekhov.billingcenter.rocketmq;

import com.alibaba.fastjson.JSON;
import com.olekhov.billingcenter.dao.billing.RocketmqTransactionLogMapper;
import com.olekhov.billingcenter.domain.entity.billing.Billing;
import com.olekhov.billingcenter.domain.entity.billing.RocketmqTransactionLog;
import com.olekhov.billingcenter.service.billing.BillingService;
import lombok.RequiredArgsConstructor;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

@RocketMQTransactionListener(txProducerGroup = "billing-center-group")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RemovePurchasedCartItemTransactionListener implements RocketMQLocalTransactionListener {
    private final BillingService billingService;
    private final RocketmqTransactionLogMapper rocketmqTransactionLogMapper;

    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        MessageHeaders headers = message.getHeaders();

        // Get Args by getting headers
        String transactionId = (String) headers.get(RocketMQHeaders.TRANSACTION_ID);
        String billingDTOJSONString = (String) headers.get("billing");

        // Deserialization: Parse JSON String back to Object
        Billing billing = JSON.parseObject(billingDTOJSONString, Billing.class);

        // Execute Local Transaction
        try {
            this.billingService.insertNewBillingWithRocketMQLog(billing, transactionId);
            return RocketMQLocalTransactionState.COMMIT;
        } catch (Exception e) {
            return RocketMQLocalTransactionState.ROLLBACK;
        }
    }

    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        MessageHeaders headers = message.getHeaders();
        String transactionId = (String) headers.get(RocketMQHeaders.TRANSACTION_ID);
        // Check if local transaction succeed by querying database
        RocketmqTransactionLog rocketmqTransactionLog = this.rocketmqTransactionLogMapper.selectOne(
                RocketmqTransactionLog.builder()
                        .transactionId(transactionId)
                        .build()
        );
        if (rocketmqTransactionLog != null) {
            return RocketMQLocalTransactionState.COMMIT;
        }
        return RocketMQLocalTransactionState.ROLLBACK;
    }

}
