package com.olekhov.billingcenter.controller.billing;

import com.olekhov.billingcenter.domain.dto.BillingDTO;
import com.olekhov.billingcenter.service.billing.BillingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/billing")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BillingListController {
    private final BillingService billingService;

    @GetMapping("/customer/{customerId}")
    public List<BillingDTO> listByCustomerId(@PathVariable Integer customerId) {
        return this.billingService.listByCustomerId(customerId);
    }

    @GetMapping("/merchant/{merchantId}")
    public List<BillingDTO> listByMerchantId(@PathVariable Integer merchantId) {
        return this.billingService.listByMerchantId(merchantId);
    }

}
