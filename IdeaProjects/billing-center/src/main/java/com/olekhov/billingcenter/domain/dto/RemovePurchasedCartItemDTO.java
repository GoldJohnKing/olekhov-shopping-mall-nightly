package com.olekhov.billingcenter.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RemovePurchasedCartItemDTO {
    private Integer customerId;
    private Long itemId;
}
