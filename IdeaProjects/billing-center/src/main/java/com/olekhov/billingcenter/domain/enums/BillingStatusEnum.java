package com.olekhov.billingcenter.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BillingStatusEnum {
    unpaid, paid, shipping, arrived, canceled, refunding, refunded
}
