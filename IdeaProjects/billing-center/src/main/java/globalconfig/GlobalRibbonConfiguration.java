package globalconfig;

import com.alibaba.cloud.nacos.ribbon.NacosRule;
import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// This class MUST NOT be in the same package of the Bootstrap class
// in billing to avoid root-child context scanner issue
@Configuration
public class GlobalRibbonConfiguration {
    @Bean
    public IRule ribbonRule() {
        // Use NacosRule as global default Ribbon rule
        // This rule priorities valid services in the same cluster
        // It would downgrade to all valid services if there's no valid service in the same cluster
        return new NacosRule();
    }
}
