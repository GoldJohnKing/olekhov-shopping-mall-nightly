package com.olekhov.usercenter.dao.user;

import com.olekhov.usercenter.domain.entity.user.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}