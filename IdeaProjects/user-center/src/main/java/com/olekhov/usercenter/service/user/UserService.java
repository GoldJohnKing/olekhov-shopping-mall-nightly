package com.olekhov.usercenter.service.user;

import com.olekhov.usercenter.dao.user.UserMapper;
import com.olekhov.usercenter.domain.dto.UserDTO;
import com.olekhov.usercenter.domain.dto.UserLoginDTO;
import com.olekhov.usercenter.domain.entity.user.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {
    private final UserMapper userMapper;

    public UserDTO selectById(Integer id) {
        User user = this.userMapper.selectByPrimaryKey(id);
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(user, userDTO);
        return userDTO;
    }

}
