package com.olekhov.usercenter.controller.user;

import com.olekhov.usercenter.domain.dto.UserDTO;
import com.olekhov.usercenter.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserSelectController {
    private final UserService userService;

    @GetMapping("/select/{id}")
    public UserDTO selectById(@PathVariable Integer id) {
        return this.userService.selectById(id);
    }
}
