package com.olekhov.itemcenter.controller.item;

import com.olekhov.itemcenter.domain.dto.ItemDTO;
import com.olekhov.itemcenter.service.item.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/item")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ItemSelectController {
    private final ItemService itemService;

    @GetMapping("/select/{id}")
    public ItemDTO selectById(@PathVariable Long id) {
        return this.itemService.selectById(id);
    }

}
