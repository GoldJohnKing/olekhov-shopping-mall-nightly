package com.olekhov.itemcenter.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ItemDTO {
    private Long id;
    private Integer merchantId;
    private String name;
    private BigDecimal price;
    private String status;
    private String cover;
    private String detail;

    // Properties from foreign requests
    private String merchantName;
}
