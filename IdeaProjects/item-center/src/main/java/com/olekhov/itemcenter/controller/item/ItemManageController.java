package com.olekhov.itemcenter.controller.item;

import com.olekhov.itemcenter.domain.dto.ItemDTO;
import com.olekhov.itemcenter.domain.dto.ItemInsertDTO;
import com.olekhov.itemcenter.domain.dto.ItemUpdateDTO;
import com.olekhov.itemcenter.domain.entity.item.Item;
import com.olekhov.itemcenter.service.item.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/item")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ItemManageController {
    private final ItemService itemService;

    @GetMapping("/list/{merchantId}")
    public List<ItemDTO> selectById(@PathVariable Integer merchantId) {
        return this.itemService.listByMerchantId(merchantId);
    }

    @PutMapping("/add")
    public Item insert(@RequestBody ItemInsertDTO itemInsertDTO) {
        // TODO: Add Authorization
        return this.itemService.insert(itemInsertDTO);
    }

    @PutMapping("/edit/{id}")
    public Item updateById(@PathVariable Long id, @RequestBody ItemUpdateDTO itemUpdateDTO) {
        // TODO: Add Authorization
        return this.itemService.updateById(id, itemUpdateDTO);
    }

    @PutMapping("/delete/{id}")
    public Item deleteById(@PathVariable Long id) {
        // TODO: Add Authorization
        ItemUpdateDTO itemUpdateDTO = new ItemUpdateDTO().builder()
                .status("deleted")
                .build();
        return this.itemService.updateById(id, itemUpdateDTO);
    }

}
