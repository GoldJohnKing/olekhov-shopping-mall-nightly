package com.olekhov.itemcenter.service.item;

import com.olekhov.itemcenter.dao.item.ItemMapper;
import com.olekhov.itemcenter.domain.dto.ItemDTO;
import com.olekhov.itemcenter.domain.dto.ItemInsertDTO;
import com.olekhov.itemcenter.domain.dto.ItemUpdateDTO;
import com.olekhov.itemcenter.domain.dto.UserDTO;
import com.olekhov.itemcenter.domain.entity.item.Item;
import com.olekhov.itemcenter.feignclient.UserCenterFeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ItemService {
    private final ItemMapper itemMapper;
    private final UserCenterFeignClient userCenterFeignClient;

    public ItemDTO selectById(Long id) {
        Item item = this.itemMapper.selectByPrimaryKey(id);
        // Fetch Needed Info through Feign Client
        UserDTO userDTO = this.userCenterFeignClient.selectById(item.getMerchantId());
        // Assemble DTO
        ItemDTO itemDTO = new ItemDTO();
        BeanUtils.copyProperties(item, itemDTO);
        itemDTO.setMerchantName(userDTO.getUsername());
        return itemDTO;
    }

    public List<ItemDTO> listByMerchantId(Integer merchantId) {
        // Define query example
        Example example = new Example(Item.class);
        example.createCriteria().andEqualTo("merchantId", merchantId);
        // Fetch item list from database
        List<Item> itemList = this.itemMapper.selectByExample(example);
        // Batch assemble DTO
        List<ItemDTO> itemDTOList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(itemList)) {
            for (Item item : itemList) {
                // Fetch needed info through Feign Client
                UserDTO userDTO = this.userCenterFeignClient.selectById(item.getMerchantId());
                // Assemble DTO
                ItemDTO itemDTO = new ItemDTO();
                BeanUtils.copyProperties(item, itemDTO);
                itemDTO.setMerchantName(userDTO.getUsername());
                itemDTOList.add(itemDTO);
            }
        }
        return itemDTOList;
    }

    public Item insert(ItemInsertDTO itemInsertDTO) {
        // TODO: Get current user's id
        // TODO: Check if current user has merchant permission
        Item item = new Item();
        // Assemble item
        BeanUtils.copyProperties(itemInsertDTO, item);
        // Submit changes
        this.itemMapper.insertSelective(item);
        return item;
    }

    public Item updateById(Long id, ItemUpdateDTO itemUpdateDTO) {
        // TODO: Get current user's id
        // TODO: Check if current user has merchant permission
        Item item = this.itemMapper.selectByPrimaryKey(id);
        // Check if item exists
        if (item == null) throw new IllegalArgumentException("Invalid Parameters: Item does not exist.");
        // Assemble item
        BeanUtils.copyProperties(itemUpdateDTO, item);
        // Submit changes
        this.itemMapper.updateByPrimaryKeySelective(item);
        return item;
    }

}
