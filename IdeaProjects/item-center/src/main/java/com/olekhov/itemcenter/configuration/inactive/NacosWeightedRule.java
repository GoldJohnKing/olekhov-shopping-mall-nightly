package com.olekhov.itemcenter.configuration.inactive;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.ribbon.NacosServer;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.BaseLoadBalancer;
import com.netflix.loadbalancer.Server;
import org.springframework.beans.factory.annotation.Autowired;

// Inactive
public class NacosWeightedRule extends AbstractLoadBalancerRule {
    @Autowired
    private NacosDiscoveryProperties nacosDiscoveryProperties;

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {
    }

    @Override
    public Server choose(Object o) {
        try {
            // Get LoadBalancer to obtain APIs in Ribbon
            BaseLoadBalancer loadBalancer = (BaseLoadBalancer) this.getLoadBalancer();

            // Get service names from Ribbon LoadBalancer
            String name = loadBalancer.getName();

            // Get APIs of Nacos Discovery
            NamingService namingService = nacosDiscoveryProperties.namingServiceInstance();

            // Let Nacos Discovery's LoadBalancer API chooses
            // one healthy instance according to Nacos weight
            Instance instance = namingService.selectOneHealthyInstance(name);

            // Package and return the instance as a NacosServer
            return new NacosServer(instance);

        } catch (NacosException e) {
            return null;
        }
    }
}
