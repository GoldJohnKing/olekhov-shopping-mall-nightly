package com.olekhov.itemcenter.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ItemStatusEnum {
    published,
    unpublished,
    sold,
    deleted
}
