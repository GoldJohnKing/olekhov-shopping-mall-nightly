package com.olekhov.itemcenter.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ItemInsertDTO {
    private Integer merchantId;
    private String name;
    private BigDecimal price;
    private String status;
    private String cover;
    private String detail;
}
