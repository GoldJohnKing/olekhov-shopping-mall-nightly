package com.olekhov.itemcenter.dao.item;

import com.olekhov.itemcenter.domain.entity.item.Item;
import tk.mybatis.mapper.common.Mapper;

public interface ItemMapper extends Mapper<Item> {
}