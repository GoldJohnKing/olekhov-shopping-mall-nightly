package com.olekhov.itemcenter.configuration.inactive;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.ribbon.ExtendBalancer;
import com.alibaba.cloud.nacos.ribbon.NacosServer;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alibaba.nacos.client.naming.core.Balancer;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.BaseLoadBalancer;
import com.netflix.loadbalancer.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

// Inactive
public class NacosSameClusterWeightedRule extends AbstractLoadBalancerRule {

    @Autowired
    private NacosDiscoveryProperties nacosDiscoveryProperties;

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {
    }

    @Override
    public Server choose(Object o) {
        try {
            // Get LoadBalancer to obtain almost everything in Ribbon
            BaseLoadBalancer loadBalancer = (BaseLoadBalancer) this.getLoadBalancer();

            // Get current service cluster name
            String clusterName = nacosDiscoveryProperties.getClusterName();

            // Get service names from Ribbon LoadBalancer
            String name = loadBalancer.getName();

            // Get APIs of Nacos Discovery
            NamingService namingService = nacosDiscoveryProperties.namingServiceInstance();

            // Select healthy instances from Nacos Discovery
            List<Instance> instances = namingService.selectInstances(name, true);

            // Filter out instances in the same cluster
            List<Instance> sameClusterInstances = instances.stream()
                    .filter(instance -> Objects.equals(instance.getClusterName(), clusterName)
                    ).collect(Collectors.toList());

            // Choose instances from same cluster, or else use any instances
            List<Instance> instancesToBeChosen = new ArrayList<Instance>();
            if (CollectionUtils.isEmpty(sameClusterInstances)) {
                instancesToBeChosen = instances;
            } else {
                instancesToBeChosen = sameClusterInstances;
            }

            // Let ExtendBalancer chooses an instance according to Nacos weight
            Instance instance = ExtendBalancer.getHostByRandomWeight2(instancesToBeChosen);

            // Package and return the instance as a NacosServer
            return new NacosServer(instance);

        } catch (NacosException e) {
            return null;
        }
    }
}
