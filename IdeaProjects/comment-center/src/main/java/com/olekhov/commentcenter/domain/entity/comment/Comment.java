package com.olekhov.commentcenter.domain.entity.comment;

import javax.persistence.*;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "COMMENT")
public class Comment {
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    @Column(name = "ITEM_ID")
    private Long itemId;

    @Column(name = "CUSTOMER_ID")
    private Integer customerId;

    @Column(name = "CONTENT")
    private String content;
}