package com.olekhov.commentcenter.configuration;

import globalconfig.GlobalRibbonConfiguration;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@RibbonClients(defaultConfiguration = GlobalRibbonConfiguration.class)
public class RibbonConfigurationEntry {
}
