package com.olekhov.commentcenter.service.comment;

import com.olekhov.commentcenter.dao.comment.CommentMapper;
import com.olekhov.commentcenter.domain.dto.CommentDTO;
import com.olekhov.commentcenter.domain.dto.CommentInsertDTO;
import com.olekhov.commentcenter.domain.dto.UserDTO;
import com.olekhov.commentcenter.domain.entity.comment.Comment;
import com.olekhov.commentcenter.feignclient.UserCenterFeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CommentService {
    private final CommentMapper commentMapper;
    private final UserCenterFeignClient userCenterFeignClient;

    public List<CommentDTO> listByItemId(Long itemId) {
        // Define query example
        Example example = new Example(Comment.class);
        example.createCriteria().andEqualTo("itemId", itemId);
        // Fetch comment list from database
        List<Comment> commentList = this.commentMapper.selectByExample(example);
        // Assemble DTO
        List<CommentDTO> commentDTOList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(commentList)) {
            for (Comment comment : commentList) {
                // Fetch needed info through Feign Client
                UserDTO userDTO = this.userCenterFeignClient.selectById(comment.getCustomerId());
                // Assemble DTO
                CommentDTO commentDTO = new CommentDTO();
                BeanUtils.copyProperties(comment, commentDTO);
                commentDTO.setCustomerName(userDTO.getUsername());
                commentDTOList.add(commentDTO);
            }
        }
        return commentDTOList;
    }

    public Comment insert(CommentInsertDTO commentInsertDTO) {
        // TODO: Get current user's id
        // TODO: Check if current user has valid permission
        Comment comment = new Comment();
        // Assemble item
        BeanUtils.copyProperties(commentInsertDTO, comment);
        // Submit changes
        this.commentMapper.insertSelective(comment);
        return comment;
    }

}
