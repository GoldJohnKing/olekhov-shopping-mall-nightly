package com.olekhov.commentcenter.controller.comment;

import com.olekhov.commentcenter.domain.dto.CommentDTO;
import com.olekhov.commentcenter.service.comment.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/comment")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CommentListController {
    private final CommentService commentService;

    @GetMapping("/list/{itemId}")
    public List<CommentDTO> selectById(@PathVariable Long itemId) {
        return this.commentService.listByItemId(itemId);
    }

}
