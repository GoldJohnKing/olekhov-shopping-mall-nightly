package com.olekhov.commentcenter.feignclient;

import com.olekhov.commentcenter.domain.dto.ItemDTO;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class ItemCenterFeignClientFallbackFactory implements FallbackFactory<ItemCenterFeignClient> {
    @Override
    public ItemCenterFeignClient create(Throwable throwable) {
        return new ItemCenterFeignClient() {
            @Override
            public ItemDTO selectById(Long id) {
                ItemDTO itemDTO = new ItemDTO();
                itemDTO.setName("Loading...");
                return itemDTO;
            }
        };
    }
}
