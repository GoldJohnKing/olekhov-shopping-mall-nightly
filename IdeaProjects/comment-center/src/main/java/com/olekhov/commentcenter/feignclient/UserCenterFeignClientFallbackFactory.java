package com.olekhov.commentcenter.feignclient;

import com.olekhov.commentcenter.domain.dto.UserDTO;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class UserCenterFeignClientFallbackFactory implements FallbackFactory<UserCenterFeignClient> {
    @Override
    public UserCenterFeignClient create(Throwable throwable) {
        return new UserCenterFeignClient() {
            @Override
            public UserDTO selectById(Integer id) {
                UserDTO userDTO = new UserDTO();
                userDTO.setUsername("Loading...");
                return userDTO;
            }
        };
    }
}
