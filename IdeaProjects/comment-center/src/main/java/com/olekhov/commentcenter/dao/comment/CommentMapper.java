package com.olekhov.commentcenter.dao.comment;

import com.olekhov.commentcenter.domain.entity.comment.Comment;
import tk.mybatis.mapper.common.Mapper;

public interface CommentMapper extends Mapper<Comment> {
}