package com.olekhov.commentcenter.controller.comment;

import com.olekhov.commentcenter.domain.dto.CommentInsertDTO;
import com.olekhov.commentcenter.domain.entity.comment.Comment;
import com.olekhov.commentcenter.service.comment.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comment")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CommentInsertController {
    private final CommentService commentService;

    @PutMapping("/add")
    public Comment insert(@RequestBody CommentInsertDTO commentInsertDTO) {
        // TODO: Add Authorization
        return this.commentService.insert(commentInsertDTO);
    }
}
