package com.olekhov.commentcenter.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentInsertDTO {
    private Long itemId;
    private Integer customerId;
    private String content;
}
