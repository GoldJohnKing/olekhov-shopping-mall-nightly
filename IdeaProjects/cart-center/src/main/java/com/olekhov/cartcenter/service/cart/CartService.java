package com.olekhov.cartcenter.service.cart;

import com.olekhov.cartcenter.dao.cart.CartMapper;
import com.olekhov.cartcenter.domain.dto.*;
import com.olekhov.cartcenter.domain.entity.cart.Cart;
import com.olekhov.cartcenter.feignclient.ItemCenterFeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CartService {
    private final CartMapper cartMapper;
    private final ItemCenterFeignClient itemCenterFeignClient;

    public List<CartDTO> listByUserId(Integer customerId) {
        // Define query example
        Example example = new Example(Cart.class);
        example.createCriteria().andEqualTo("customerId", customerId);
        // Fetch cart list from database
        List<Cart> cartList = this.cartMapper.selectByExample(example);
        // Assemble DTO
        List<CartDTO> cartDTOList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(cartList)) {
            for (Cart cart : cartList) {
                // Fetch needed info through Feign Client
                ItemDTO itemDTO = this.itemCenterFeignClient.selectById(cart.getItemId());
                // Assemble DTO
                CartDTO cartDTO = new CartDTO();
                BeanUtils.copyProperties(cart, cartDTO);
                cartDTO.setItemName(itemDTO.getName());
                cartDTO.setItemPrice(itemDTO.getPrice());
                cartDTO.setItemCover(itemDTO.getCover());
                cartDTO.setMerchantName(itemDTO.getMerchantName());
                cartDTOList.add(cartDTO);
            }
        }
        return cartDTOList;
    }

    public Cart insert(CartInsertDTO cartInsertDTO) {
        // TODO: Get current user's id
        // TODO: Check if current user has customer permission
        Cart cart = new Cart();
        // Assemble item
        BeanUtils.copyProperties(cartInsertDTO, cart);
        // Submit changes
        this.cartMapper.insertSelective(cart);
        return cart;
    }

    public Cart updateById(Long id, CartUpdateDTO cartUpdateDTO) {
        // TODO: Get current user's id
        // TODO: Check if cart item belongs to current user
        Cart cart = this.cartMapper.selectByPrimaryKey(id);
        // Check if cart item exists
        if (cart == null) throw new IllegalArgumentException("Invalid Parameters: Cart item does not exist.");
        // Assemble item
        BeanUtils.copyProperties(cartUpdateDTO, cart);
        // Submit changes
        this.cartMapper.updateByPrimaryKeySelective(cart);
        return cart;
    }

    public int deleteById(Long id) {
        // TODO: Get current user's id
        // TODO: Check if current user has customer permission
        return this.cartMapper.deleteByPrimaryKey(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletePurchasedCartItemByExample(RemovePurchasedCartItemDTO removePurchasedCartItemDTO) {
        // Define query example
        Example example = new Example(Cart.class);
        example.createCriteria()
                .andEqualTo("customerId", removePurchasedCartItemDTO.getCustomerId())
                .andEqualTo("itemId", removePurchasedCartItemDTO.getItemId());
        // Delete purchased cart item
        this.cartMapper.deleteByExample(example);
    }

}
