package com.olekhov.cartcenter.rocketmq;

import com.olekhov.cartcenter.domain.dto.RemovePurchasedCartItemDTO;
import com.olekhov.cartcenter.service.cart.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RemovePurchasedCartItemStreamConsumer {
    private final CartService cartService;

    @StreamListener(Sink.INPUT)
    public void receive(RemovePurchasedCartItemDTO removePurchasedCartItemDTO) {
        this.cartService.deletePurchasedCartItemByExample(removePurchasedCartItemDTO);
    }
}
