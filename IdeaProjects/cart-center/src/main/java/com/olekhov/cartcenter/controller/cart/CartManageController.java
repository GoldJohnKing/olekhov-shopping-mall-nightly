package com.olekhov.cartcenter.controller.cart;

import com.olekhov.cartcenter.domain.dto.CartDTO;
import com.olekhov.cartcenter.domain.dto.CartInsertDTO;
import com.olekhov.cartcenter.domain.dto.CartUpdateDTO;
import com.olekhov.cartcenter.domain.entity.cart.Cart;
import com.olekhov.cartcenter.service.cart.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CartManageController {
    private final CartService cartService;

    @GetMapping("/list/{customerId}")
    public List<CartDTO> selectById(@PathVariable Integer customerId) {
        return this.cartService.listByUserId(customerId);
    }

    @PutMapping("/add")
    public Cart insert(@RequestBody CartInsertDTO cartInsertDTO) {
        // TODO: Add Authorization
        return this.cartService.insert(cartInsertDTO);
    }

    @PutMapping("/edit/{id}")
    public Cart updateById(@PathVariable Long id, @RequestBody CartUpdateDTO cartUpdateDTO) {
        // TODO: Add Authorization
        return this.cartService.updateById(id, cartUpdateDTO);
    }

    @PutMapping("/delete/{id}")
    public int deleteById(@PathVariable Long id) {
        // TODO: Add Authorization
        return this.cartService.deleteById(id);
    }

}
