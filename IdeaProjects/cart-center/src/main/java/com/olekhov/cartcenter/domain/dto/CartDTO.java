package com.olekhov.cartcenter.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CartDTO {
    private Long id;
    private Integer customerId;
    private Long itemId;
    private Integer count;

    // Properties from foreign requests
    private String itemName;
    private BigDecimal itemPrice;
    private String itemStatus;
    private String merchantName;
    private String itemCover;
}
