package com.olekhov.cartcenter.dao.cart;

import com.olekhov.cartcenter.domain.entity.cart.Cart;
import tk.mybatis.mapper.common.Mapper;

public interface CartMapper extends Mapper<Cart> {
}