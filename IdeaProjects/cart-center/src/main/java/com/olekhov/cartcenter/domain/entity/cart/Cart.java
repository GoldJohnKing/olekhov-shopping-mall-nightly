package com.olekhov.cartcenter.domain.entity.cart;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CART")
public class Cart {
    @Id
    @Column(name = "ID")
    @GeneratedValue(generator = "JDBC")
    private Long id;

    @Column(name = "CUSTOMER_ID")
    private Integer customerId;

    @Column(name = "ITEM_ID")
    private Long itemId;

    @Column(name = "COUNT")
    private Integer count;
}